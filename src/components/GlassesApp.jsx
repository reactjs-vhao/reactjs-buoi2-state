import React, { useState } from "react";
import styles from "./style.module.css";
import dataGlasses from "../data/dataGlasses.json";

const GlassesApp = () => {
  const [glasses, setGlasses] = useState(dataGlasses[0]);
  return (
    <div className={styles.container}>
      <h1>TRY GLASSES APP ONLINE</h1>
      <div className={styles.model}>
        <img src={glasses.url} alt="" />
        <div className={styles["model-text"]}>
          <h2>{glasses.name}</h2>
          <p>{glasses.desc}</p>
        </div>
      </div>
      <div className={styles["image-wrap"]}>
        {dataGlasses.map((item, index) => (
          <img
            key={index}
            src={item.url}
            alt=""
            onClick={() => {
              setGlasses(dataGlasses[index]);
            }}
          />
        ))}
      </div>
    </div>
  );
};

export default GlassesApp;
