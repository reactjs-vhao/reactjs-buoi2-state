import React from "react";
import "./App.css";
import GlassesApp from "./components/GlassesApp";

const App = () => (
  <div className="App">
    <GlassesApp />
  </div>
);

export default App;
